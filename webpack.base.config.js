const path = require('path');
const fs = require("fs");
const webpack = require('webpack');
const globImporter = require('node-sass-glob-importer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const glob = require('glob');


const PATHS = {
	app: path.join(__dirname, './app'),
	build: path.join(__dirname, './build'),
	appAssets: path.join(__dirname, './app/assets'),
	buildAssets: path.join(__dirname, './build/assets'),
	assets: 'assets'
}

const PAGES_DIR = `${PATHS.app}/pages/`;
let PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.pug'));
let singlePageName = '';
let entries = '*.js';

if (process.argv[process.argv.length - 1].substring(0,6) === '--page') {
  singlePageName = process.argv[process.argv.length - 1].split('=')[1];
  PAGES = [singlePageName + '.pug'];
  entries = singlePageName + '.js';
}

module.exports = {
	externals: {
		paths: PATHS,
		pages: PAGES,
	},
  	entry: toObject(glob.sync(`${PATHS.app}/js/${entries}`)),
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: `js/[name].js`,
		publicPath: '/'
	},
	optimization: {
		moduleIds: 'named',
		splitChunks: {
			cacheGroups: {
				common: {
					name: 'common',
					test: /common/,
					chunks: 'all',
				},
				vendor: {
					name: 'libs',
					test: /node_modules/,
					chunks: 'all',
					enforce: true
				}
			}
		},
	},
	module: {
		rules: [
			{
				// pug
				test: /\.pug$/,
				loader: 'pug-loader',
				query: {
					pretty: true
				}
			},
			{
				// js
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				// fonts
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader',
				exclude: [path.resolve(__dirname, "./app/assets/images")],
				options: {
					name: 'assets/fonts/[name].[ext]'
				}
			},
			{
				// php
				test: /\.php$/,
				loader: 'file-loader'
			},
			{
				// json
				test: /\.json$/,
				loader: 'file-loader'
			},
			{
				// scss
				test: /\.scss/,
				use: [
					'style-loader',
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
							sassOptions: {
								importer: globImporter()
							}
						}
					}
				]
			},
			{
				// css
				test: /\.css/,
				use: [
					'style-loader',
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				// images
				test: /\.(png|jpe?g|gif|svg|webp)$/,
				loader: 'file-loader',
				exclude: [path.resolve(__dirname, "./app/assets/fonts")],
				options: {
					name: 'assets/images/[folder]/[name].[ext]'
				}
			},
			{
				// php
				test: /\.php?$/,
				loader: 'file-loader',
				options: {
					name: '[name].[ext]'
				}
			}
		]
	},
	resolve: {
		alias: {
			'~': PATHS.app,
			'~components': `${PATHS.app}/components`,
			'~node': 'node_modules/',
		}
	},
	plugins: [

		new webpack.ProvidePlugin({
			jQuery          : "jquery",
			jquery          : "jquery",
			$               : "jquery",
			"window.jQuery" : "jquery",
			"window.jquery" : "jquery",
			"window.$"      : "jquery"
		}),

		// формирование css файла
		new MiniCssExtractPlugin({
			filename: `css/[name].css`
		}),

		// Очистка билда при локальной работе
		new CleanWebpackPlugin(),

		// сборка pug + json в html
		...PAGES.map(page => new HtmlWebpackPlugin({
			template: `${PAGES_DIR}/${page}`,
			filename: `./${page.replace(/\.pug/,'.html')}`,
			templateParameters: require(`${PATHS.app}/data/${page.replace(/\.pug/,'.json')}`),
			minify: false,
			inject: false,
			scriptLoading: 'defer',
			chunks: ['common', 'libs', `${page.replace(/\.pug/,'')}`],
			// hash: true
		} )),

		// Спрайт svg
		new SVGSpritemapPlugin(`${PATHS.appAssets}/images/svg/*.svg`, {
			output: {
				filename: `${PATHS.assets}/images/required/sprite.svg`,
				svg4everybody: true,
				svgo: {
					removeTitle: true,
					removeStyleElement: true,
					cleanupNumericValue: true,
				}
			},
			sprite: {
				prefix: false
			}
		}),

		// копирование файлов из app в build
		new CopyWebpackPlugin({
			patterns: [
				{
					from: `${PATHS.appAssets}/images`,
					globOptions: {
						dot: true,
						ignore: [
							'**/svg/**'
						]
					},
					to:   `${PATHS.buildAssets}/images`,
				},
				{
					from: `${PATHS.appAssets}/fonts`,
					to:   `${PATHS.buildAssets}/fonts`
				},
				{
					from: `${PATHS.app}/ajax`,
					to:   `${PATHS.build}/ajax`
				}
			]
		}),
	]
}

function toObject(paths) {
	const entry = {};
	paths.forEach(function(p) {
		const name = path.basename(p, '.js');
		entry[name] = p;
	});
	return entry;
}
