module.exports = {
	plugins: [
		require('autoprefixer'),
    require('postcss-inline-svg')({
      removeFill: true,
      path: './app/assets/images/svg/'
    }),
    // require('css-mqpacker')({
		// 	sort: false
		// }),
	]
};
