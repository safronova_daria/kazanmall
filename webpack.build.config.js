const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.config');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlCriticalWebpackPlugin = require("html-critical-webpack-plugin");
const ImageminWebpackPlugin = require("imagemin-webpack-plugin").default;
const ImageminWebP = require("imagemin-webp");

const buildWebpackConfig = merge(baseWebpackConfig, {
	mode: 'production',
	devtool: 'source-map',
	output: {
		publicPath: '/norgau/build/',
	},
	optimization: {
		minimize: false
	},
	plugins: [
		new PreloadWebpackPlugin({
			rel: 'preload',
			as: 'font',
			include: 'allAssets',
			fileWhitelist: [/\DidotLT-Bold.woff2/, /\ProximaNova-Regular.woff2/, /\ProximaNova-Semibold.woff2/, /\ProximaNova-Bold.woff2/]
		}),

		new CompressionPlugin({
			test: /\.js$|\.css$|\.html$/,
			algorithm: "gzip"
		}),
	]
})

module.exports = new Promise((resolve, reject) => {
	resolve(buildWebpackConfig)
} )

