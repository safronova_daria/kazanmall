import 'jquery';

global.jQuery = $;
global.jquery = $;
global.$ = $;

import svg4everybody from 'svg4everybody/dist/svg4everybody';
import objectFitImages from 'object-fit-images';
import LazyLoad from 'lazyload/lazyload';

import '../scss/common.scss';

$(document).ready(function() {
    // adds SVG External Content support to all browsers
    svg4everybody();

    // Polyfill object-fit/object-position on <img>
    objectFitImages();

    // lazyload
    let images = document.querySelectorAll('img.lazyload');
    new LazyLoad(images);

    window.LazyLoad = LazyLoad;

    let lowQualityImg = $('[data-img-min]');
    lowQualityImg.each(function() {
        if ($(this).is('source'))
            $(this).prop('srcset', $(this).attr('data-src'));
        else
            $(this).prop('src', $(this).attr('data-src'));
    });
});

$(window).on('load', function() {
    $('body').addClass('loaded');

    // загрузка изображений методом LazyLoad
    setTimeout(() => {
        let img = document.querySelectorAll('img.load');
        let lazy = new LazyLoad(img);
        lazy.loadImages();
    }, 1000);
});

import '~components/header/header';
import '~components/footer/footer';
import '~components/cookie/cookie';
import '~components/popup/popup';
import '~components/forms/input/input';
import '~components/forms/textarea/textarea';
import '~components/slider/slider';
import '~components/dropdown/dropdown';
import '~components/hamburger/hamburger';
import '~components/forms/form-validation/form-validation';