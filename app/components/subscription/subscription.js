import 'jquery-validation';

class Subscription {
  constructor(elem) {
    this.subscription = elem;
  }

  initialize() {
    let subscription = this.subscription;
    let subscriptionForm = subscription.find('[data-subscription-form]');
    let subscriptionBtn = subscription.find('[data-subscription-btn]');
    let subscriptionInput = subscription.find('[data-subscription-input]');
    let subscriptionThanks = subscription.find('[data-subscription-thanks]');
    let subscriptionCheckbox = subscription.find('[data-subscription-checkbox]');

    subscriptionForm.on('submit', function(e) {
      e.preventDefault();
    });

    subscriptionBtn.on('click', function() {
      if(subscriptionBtn.hasClass('active')) {
        subscriptionInput.toggleClass('is-hidden');
        subscriptionThanks.toggleClass('is-hidden');
        subscriptionBtn.removeClass('active');
        subscriptionInput.prev().addClass('is-hidden');
        subscriptionInput.parent().css('padding', 0);
      }
    });

    subscriptionInput.on('input', function() {
      if(!subscriptionInput.val() == '') {
        subscriptionInput.prev().removeClass('is-hidden');
      } else {
        subscriptionInput.prev().addClass('is-hidden');
      }
    });

    subscriptionForm.on('keyup change paste', 'input, select, textarea', function() {
      if(!subscriptionInput.hasClass('error') && !subscriptionInput.val() == '' 
        && subscriptionCheckbox.is(':checked') && subscriptionThanks.hasClass('is-hidden')) {
        subscriptionBtn.addClass('active');
      } else {
        subscriptionBtn.removeClass('active');
      }
    });

    jQuery.validator.addMethod(
      'emailfull',
      function (value, element) {
        return (
          this.optional(element) ||
          /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(
            value
          )
        );
      },
      'Некорректные данные'
    );

    subscriptionForm.validate({
      rules: {
        email: {
          emailfull: true
        }
      },
      errorClass: 'error'
    });
  }
}

export { Subscription };

$(window).on('load', () => {
  let subscriptionElements = [];
  if ($('[data-subscription]').length > 0) {
    $('[data-subscription]').each(function (i) {
      subscriptionElements[i] = new Subscription($(this));
      subscriptionElements[i].initialize();
    });
  }
});
