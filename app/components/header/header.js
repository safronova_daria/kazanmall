window.onload = function() {
    let search = document.querySelector('.header__search');
    let searchBtn = document.querySelector('.header__search-btn');
    let searchInput = document.querySelector('.header__search-form-input');
    
    let menu = document.querySelector('.header__menu');
    let form = document.querySelector('.header__search-form');
    
    let headerWrapperInfo = document.querySelector('.header__wrapper-info');
    let headerWrapperMain = document.querySelector('.header__wrapper-main');
    let headerEntertaniments = document.querySelector('.header__entertainments');
    
    
    function OpenSearch(e) {
        menu.classList.toggle("is-hidden");
        form.classList.toggle("is-hidden");
        searchBtn.classList.toggle("is-open");
        search.classList.toggle("is-open");
        
        searchInput.value = "";
    }
    
    function ScrollHeader(e) {
        let scrollTop = window.scrollY;
        
        if(scrollTop > 0) {
            headerWrapperInfo.classList.add("is-hidden");
            headerWrapperMain.classList.add("is-hidden");
            headerEntertaniments.classList.remove("is-hidden");
        }
        else {
            headerWrapperInfo.classList.remove("is-hidden");
            headerWrapperMain.classList.remove("is-hidden");
            headerEntertaniments.classList.add("is-hidden");
        }
    }
    
    form.onsubmit = function (evt) {
        evt.preventDefault();
    }
    
    searchBtn.addEventListener("click", OpenSearch);
    window.addEventListener("scroll", ScrollHeader);
}