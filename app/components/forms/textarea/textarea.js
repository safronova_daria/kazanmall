class Textarea {
  constructor(elem) {
    this.textarea = elem;
  }

  initialize() {
    this.textarea.on("input", function () {
      this.style.height = "auto";
      this.style.height = (this.scrollHeight) + "px";
    });
  }
}

export { Textarea };

$(window).on('load', () => {
  if ($('[data-textarea]').length > 0) {
    $('[data-textarea]').each(function () {
      let textarea = new Textarea($('[data-textarea]'));
      textarea.initialize();
    });
  }
});
