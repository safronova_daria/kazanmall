import 'jquery-validation';
import Inputmask from 'inputmask/dist/inputmask.min';

class ValidateForm {
  constructor(elem) {
    this.form = elem;
  }

  initialize() {
    var tel = document.querySelectorAll('input[type=tel]');
    Inputmask({
      mask: '+7(999)999-99-99',
      clearMaskOnLostFocus: true,
      placeholder: '_',
      showMaskOnHover: false,
      showMaskOnFocus: true,
      greedy: false,
      onincomplete: function () {},
    }).mask(tel);

    jQuery.extend(jQuery.validator.messages, {
      required: '',
      email: 'Пожалуйста, введите корректную почту',
      minlength: jQuery.validator.format('Недостаточно символов'),
    });

    $.validator.addMethod(
      'lettersonly',
      function (value, element) {
        return this.optional(element) || /^[а-я\s]+$/i.test(value);
      },
      'Пожалуйста, введите корректное имя'
    );

    jQuery.validator.addMethod(
      'emailfull',
      function (value, element) {
        return (
          this.optional(element) ||
          /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(
            value
          )
        );
      },
      'Некорректные данные'
    );

    jQuery.validator.addMethod(
      'phonefull',
      function (value, element) {
        return (
          this.optional(element) ||
          /^\+7\s?\(\d{3}\)\s?\d{3}(-\d{2}){2}$/i.test(value)
        );
      },
      'Пожалуйста, введите корректный номер'
    );

    jQuery.validator.addMethod(
      'errorfile',
      function (value, element) {
        let validExtensionList = ['doc', 'pdf', 'txt', 'docx', 'odt', 'rtf'];
        return (
          this.optional(element) ||
          ($.inArray(
            $(element).attr('extension').toLowerCase(),
            validExtensionList
          ) !== -1 &&
            +$(element).attr('size').toLowerCase() < 20000000)
        );
      },
      'Файл должен иметь текстовый формат (doc, pdf, txt, docx, odt, rtf)'
    );

    jQuery.validator.addMethod(
      'fileSizeCheck',
      function (value, element, param) {
        return element.files[0].size < 20000000;
      },
      'Файл не должен и превышать 20 Мб'
    );

    this.form.submit(false).validate({
      rules: {
        email: {
          required: {
            required: true,
          },
          emailfull: true,
        },
        tel: {
          required: {
            required: true,
          },
          phonefull: true,
        },
        name: {
          required: {
            required: true,
          },
          lettersonly: true,
          minlength: 3,
          normalizer: function (value) {
            return $.trim(value);
          },
        },
        textarea: {
          required: true,
          minlength: 10,
          normalizer: function (value) {
            return $.trim(value);
          },
        },

        filearea: {
          required: {
            required: true,
          },
          fileSizeCheck: true,
        },

        file: {
          errorfile: true,
        },
      },
      errorClass: 'error',
      messages: {
        // email: 'Укажите корректную электронную почту',
        // name: 'Заполните поле',
        textarea: 'Оставьте комментарий',
        phone: 'Введите номер телефона',
        tel: 'Введите корректный номер телефона',
        mk: 'Ведите серийный номер товара',
        type: 'Выберите вид инструмента',
        filearea: 'Необходимо указать файл не более 20 Мб',
      },
    });

    $('[data-file]').on('change drop', function () {
      $(this).valid();
    });

    this.form.submit(function () {
      if ($(this).valid()) {
        $(this).find('[data-preloader]').removeClass('is-hide');
        setTimeout(() => {
          $.magnificPopup.open({
            items: {
              src: '#popup-modal',
              type: 'inline',
            },
            fixedContentPos: true,
            removalDelay: 300,
            mainClass: 'mfp-fade',
            overflowY: 'scroll',
          });
          $(this).find('[data-preloader]').addClass('is-hide');
        }, 2000);
      }
    });
  }
}

export { ValidateForm };

$(window).on('load', () => {
  let forms = [];
  if ($('[data-validate]').length > 0) {
    $('[data-validate]').each(function (i) {
      forms[i] = new ValidateForm($(this));
      forms[i].initialize();
    });
  }
});
