class selectCustom {

  constructor(elem) {
    this.select = elem;
    this.label = this.select.find('[data-select-current]');
    this.container = this.select.find('[data-select-container]');
    this.options = this.select.find('[data-select-custom-option]');
	}

  initialize() {
    const that = this;

    this.select.on('click', function(e) {
      e.stopPropagation();
      that.container.toggleClass('open');
    });

    this.options.on('click', function(e) {
      e.stopPropagation();
      let text = $(this).text();
      that.label.text(text);
      that.label.attr('value', text);
      that.label.addClass('selected');

      that.container.removeClass('open');
    });

    $(window).on('click', function() {
      that.container.removeClass('open');
    })
	}
}

$(document).ready(function () {
  let select = [];

  if ($('[data-select-custom]').length > 0) {
    $('[data-select-custom]').each(function (i) {
      select[i] = new selectCustom($(this));
      select[i].initialize();
    });
  }
})