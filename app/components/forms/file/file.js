class File {
  constructor(elem) {
    this.file = elem;
    this.name = elem.attr('name');
    if (!window.files) {
      window.files = {};
    }
    window.files[this.name] = [];
  }

  initialize() {
    const that = this;
    const component = this;
    let files = this.file;
    let form = files.closest('form');
    let upload = form.find('[data-file-upload]');
    let fileLabel = form.find('[data-file-label]');
    let maxFiles = parseInt(upload.attr('data-file-max'));
    let totalCount = 0;
    let typePage = 'review';
    let isCorrectFile;

    if (window.location.pathname === '/support/feedback/') {
      typePage = 'feedback';
    }

    // добавление фото в список файлов
    files.on('change', (e) => {
      const filesCount = files.get(0).files.length;
      let fileName = files.get(0).files[filesCount - 1].name;

      if (upload.find('[data-file-inner]').length < 1) {
        upload.append('<div class="file__inner" data-file-inner></div>');
      }

      const file = files.get(0).files[filesCount - 1];

      let currentExtension = file.name.split('.').pop().toLowerCase();
      let currentSize = file.size;

      if (currentSize < 20000000) {
        window.files[component.name].push(file);
        const reader = new FileReader();

        reader.onload = function (e) {
          component.uploadFile(
            e.target.result,
            upload,
            typePage,
            fileName,
            currentExtension,
            currentSize
          );
        };
        reader.readAsDataURL(file);

        fileLabel.addClass('d-none');
      }
    });

    // удаление фото
    $(document).on('click', '[data-file-remove]', function (e) {
      let el = $(this);
      let item = el.closest('[data-file-item]');
      let inner = el.closest('[data-file-inner]');
      files.attr('type', 'text');
      files.attr('type', 'file');

      window.files[component.name].splice(item.index(), 1);
      item.remove();
      let length = inner.find('[data-file-item]').length;
      totalCount = length;

      if (length == 0) {
        inner.remove();
      }

      fileLabel.removeClass('d-none');

      e.preventDefault();
    });

    form.on('dragover, dragenter', function (e) {
      e.preventDefault();
    });

    form.on('drop', function (e) {
      e.preventDefault();
      files.files = e.originalEvent.dataTransfer.files;

      const dT = new DataTransfer();
      dT.items.add(e.originalEvent.dataTransfer.files[0]);

      files.files = dT.files;

      let fileName = files.files[0].name;

      if (upload.find('[data-file-inner]').length < 1) {
        upload.append('<div class="file__inner" data-file-inner></div>');
      }

      const file = files.files[0];

      let currentExtension = file.name.split('.').pop().toLowerCase();
      let currentSize = file.size;

      if (currentSize < 20000000) {
        window.files[component.name].push(file);
        const reader = new FileReader();

        reader.onload = function (e) {
          component.uploadFile(
            e.target.result,
            upload,
            typePage,
            fileName,
            currentExtension,
            currentSize
          );
        };
        reader.readAsDataURL(file);

        fileLabel.addClass('d-none');
      }
    });
  }

  uploadFile(src, upload, page, name, extension, size) {
    upload.find('[data-file-inner]').append(`
    <div class="file__item" data-file-item>
      <div class="file__content">
        <div class="input__wrap file__content-input">
          <div class="input__container">
            <div class="file__icon--added">
              <svg class="icon file-icon added" aria-hidden="true">
                <use xlink:href="./assets/images/required/sprite.svg#file-icon"></use>
              </svg>
            </div>
            <input readonly="readonly" name="file" class="input-form input-form--file" type="text" size=${size} extension=${extension} value="${name}" name="${this.name}_CAPTION[]" placeholder=" " data-file-input>
          </div>
        </div>
        <a href="#" class="link link--monza file__item-delete" data-file-remove></a>
      </div>
    </div>
    `);
  }

  declension(value, words) {
    value = Math.abs(value) % 100;
    var num = value % 10;
    if (value > 10 && value < 20) return words[2];
    if (num > 1 && num < 5) return words[1];
    if (num == 1) return words[0];
    return words[2];
  }
}

export { File };

$(window).on('load', () => {
  let file = [];

  if ($('[data-file]').length > 0) {
    $('[data-file]').each(function (i) {
      file[i] = new File($(this));
      file[i].initialize();
    });
  }
});
