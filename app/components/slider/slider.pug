mixin slider(data)
  //- модификатор
  +modif('slider', data.mod, data.prefix)

  //- префикс
  +prefix('slider', data.prefix)

  //- атрибуты
  - count = {'data-count': data.count} || ''
  - loop = {'data-loop': data.loop} || ''
  - autoplay = {'data-autoplay': data.autoplay} || ''
  - freeMode = {'data-freeMode': data.freeMode} || ''
  - centered = {'data-centered': data.centered} || ''
  - column = {'data-column': data.column} || ''
  - effect = {'data-effect': data.effect} || ''
  - attr = data.attr || {'data-slider': ''}
  - direction = {'data-direction': data.direction} || ''
  - touchSwipe = {'data-touchSwipe': data.touchSwipe} || ''
  - mousewheel = {'data-mousewheel': data.mousewheel} || ''
  - speed = {'data-speed': data.speed} || ''
  - pagintaionType = {'data-paginationType': data.pagintaionType} || ''

  - prefix = pr

  div(class=`slider${prefix}__container`)
    div(class=`slider${prefix}${mod} swiper-container` class=data.class )&attributes(attr, count, loop, autoplay, freeMode, centered, column, effect, direction, touchSwipe, mousewheel, speed, pagintaionType)
      div(class=`slider${prefix}__wrapper swiper-wrapper` data-slider-wrapper)
        each item in data.list
          if (data.hrefFull)
            a(class=`slider${prefix}__slide swiper-slide` class=item.class href=data.hrefFull)&attributes(item.attr)
              if (data.prefix == "vertical-scroll")
                +verticalSliderContent(data, item)
              else if (data.prefix == "menu")
                +menuSliderContent(data, item)
              else if (data.default)
                +sliderInner(data, item)
              else
                +horizontalSliderContent(data, item)
          else
            div(class=`slider${prefix}__slide swiper-slide` class=item.class)&attributes(item.attr)
              if (data.prefix == "vertical-scroll")
                +verticalSliderContent(data, item)
              else if (data.prefix == "menu")
                +menuSliderContent(data, item)
              else if (data.default)
                +sliderInner(data, item)
              else
                +horizontalSliderContent(data, item)

    if (data.pagination)
      div(class=`slider${prefix}__pagination swiper-pagination`)

    if (data.arrow)
      button(class=`slider${prefix}__button slider${prefix}__button--prev swiper-button-prev`)
      button(class=`slider${prefix}__button slider${prefix}__button--next swiper-button-next`)
        if (!data.noSvg)
          svg(width="64", height="64", version="1.1", xmlns="http://www.w3.org/2000/svg", xmlns:xlink="http://www.w3.org/1999/xlink")
            circle( fill="none" class=`slider${prefix}__button-timer-circle` stroke-width="1.5" cx="32" cy="32" r="31" stroke="#009DE0" stroke-dasharray="200" stroke-dashoffset="200")

    if (data.counter)
      div(class=`slider${prefix}__counter`)
        p(class=`slider${prefix}__counter-current` data-slider-current) 1
        span /
        p(class=`slider${prefix}__counter-amount` data-slider-amount)!=data.list.length

    if (data.scrollbar)
      div(class=`slider${prefix}__scrollbar swiper-scrollbar`)

mixin horizontalSliderContent(data, item)
    .row.align-items-center(class=item.topClass)
      if (item.title)
        div(class=`slider${prefix}__title` class=item.title.class)!= item.title.text

      if (item.text)
        div(class=`slider${prefix}__text` class=item.text.class)!= item.text.text

    if (item.img)
      div(class=`slider${prefix}__images` class=item.imgClass)&attributes(item.imgAttr)
        +img(item.img, `slider${prefix}__img lazyload`)

    if (item.picture)
      div(class=`slider${prefix}__images` class=item.imgClass)&attributes(item.imgAttr)
        +picture(item.picture)

    if (item.imgText)
      div(class=`slider${prefix}__text` class=item.imgText.class)!= item.imgText.text

    if (item.news)
      +news(item.news)

    if (item.banner)
      +banner(item.banner)

    if (item.btn)
      div(class=`slider${prefix}__btn` class=item.btn.containerClass)
        if (item.btn.href)
          a(href=item.btn.href class=item.btn.class)!= item.btn.text
        else 
          button(class=item.btn.class)!= item.btn.text

mixin sliderInner(data, item)
  div(class=`slider${prefix}__inner`)
    if (item.title)
        div(class=`slider${prefix}__title` class=item.title.class)!= item.title.text

    if (item.text)
      div(class=`slider${prefix}__text` class=item.text.class)!= item.text.text

  if (item.img)
    div(class=`slider${prefix}__images`)&attributes(item.imgAttr)
      +img(item.img, `slider${prefix}__img lazyload ${item.imgClass}`)

  if (item.picture)
    div(class=`slider${prefix}__images`)&attributes(item.imgAttr)
      +picture(item.picture)

mixin verticalSliderContent(data, item)
  .row
    .col-12.col-lg-7.col-xl-6.px-2.px-lg-8.pl-xl-9.pr-xl-0.mb-0.mb-lg-10.mb-xl-0.order-2.order-lg-1
      if (item.img)
        div(class=`slider${prefix}__images`)&attributes(item.imgAttr)
          +img(item.img, `slider${prefix}__img lazyload`)

      if (item.picture)
        div(class=`slider${prefix}__images`)&attributes(item.imgAttr)
          +picture(item.picture)

      if (item.menu)
        .row.align-items-center.pt-1.pt-md-5.pt-lg-4.justify-content-between.justify-content-lg-start
          each menuItem, index in item.menu
            div(class=`slider${prefix}__menu` class=menuItem.class)
              div(class=menuItem.title.class)!=menuItem.title.text
              div(class=menuItem.text.class)!=menuItem.text.text
            //- Разделительные квадраты внутри
            if (index != item.menu.length - 1)
              +icon('rectangle', 'col-1 col-lg-2 pl-1 pl-lg-2 rectangle--sm')

    .col-12.col-lg-5.pl-lg-5.pl-xl-17.order-1.order-lg-2.mb-6.mb-lg-0
      if (item.title)
        div(class=`slider${prefix}__title` class=item.title.class)!= item.title.text

      if (item.text)
        div(class=`slider${prefix}__text` class=item.text.class)!= item.text.text

mixin menuSliderContent(data, item)
  if (item.img)
    div(class=`slider${prefix}__images`)&attributes(item.imgAttr)
      +img(item.img, `slider${prefix}__img lazyload`)

  if (item.title)
      div(class=`slider${prefix}__title` class=item.title.class)&attributes(item.title)!= item.title.text

  if (item.text)
    div(class=`slider${prefix}__text` class=item.text.class)!= item.text.text

  a(class=`slider${prefix}__button-info` href="#").btn.btn--primary Смотреть линейку


