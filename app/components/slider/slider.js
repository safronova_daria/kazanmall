import Swiper from 'swiper';

class Slider {
  constructor(elem, index = 0) {
    this.slider = elem;
    this.index = index;
    this.pagination = this.slider.siblings('.swiper-pagination');
    this.next = this.slider.siblings('.swiper-button-next');
    this.prev = this.slider.siblings('.swiper-button-prev');
    this.scrollbar = this.slider.find('.swiper-scrollbar');
    this.loop = this.slider.is('[data-loop]') || false; // зацикливание
    this.slidesPerView = this.slider.attr('data-count') || 'auto'; // количество слайдов
    this.spaceBetween = this.slider.attr('data-space-between') === 'style' ? 0 : this.slider.attr('data-space-between') || 50; // расстояние между слайдами
    this.freeMode = this.slider.is('[data-freeMode]') || false; // свободное перетаскивание
    this.centeredSlides = this.slider.is('[data-centered]') || false; // центирование слайдов
    this.slidesPerColumn = this.slider.attr('data-column') || 1; // слайды в колонках
    this.effect = this.slider.attr('data-effect') || 'slide';
    this.autoHeight = this.slider.is('[data-auto-height]') || false;
    this.direction = this.slider.attr('data-direction') || 'horizontal';
    this.touchSwipe = this.slider.is('[data-touchSwipe]') || false;
    this.mousewheel = this.slider.is('[data-mouseWheel]') || false;
    this.paginationType = this.slider.attr('data-paginationType') || 'bullets';
    this.speed = parseInt(this.slider.attr('data-speed')) || 1000;
    this.autoplay = '';
    this.currentSlider = undefined;

    this.ww = $(window).width();
  }

  initialize() {
    let index = this.index;

    const that = this;

    let item = this.slider.find('[data-slider-wrapper]').children().length;

    if (this.slider.is('[data-autoplay]')) {
      this.autoplay = {
        delay: 4000,
        disableOnInteraction: false,
      };
    }

    if (this.slidesPerView > 0) {
      this.spaceBetween = 0;
    }

    if (item < 2) {
      this.loop = false;
      this.autoplay = '';
      this.pagination.hide();
    }

    let slider = undefined;

    if (this.slider.is('.slider-menu')) {
      let menuItem = [];
      this.slider.find('.slider-menu__title').each(function () {
        menuItem.push($(this).attr('text').toUpperCase());
      });
      slider = new Swiper(this.slider, {
        lazy: true,
        speed: this.speed,
        autoHeight: this.autoHeight,
        loop: this.loop,
        slidesPerView: this.slidesPerView,
        spaceBetween: this.spaceBetween,
        freeMode: this.freeMode,
        centeredSlides: this.centeredSlides,
        slidesPerColumn: this.slidesPerColumn,
        effect: this.effect,
        autoplay: this.autoplay,
        direction: this.direction,
        simulateTouch: this.touchSwipe,
        fadeEffect: {
          crossFade: true,
        },
        mousewheel: {
          enabled: this.mousewheel,
          releaseOnEdges: true,
        },

        pagination: {
          el: this.pagination,
          clickable: true,
          type: 'custom',
          renderCustom: function (swiper, current, total) {
            var text = '<ul>';
            for (let i = 1; i <= total; i++) {
              if (current == i) {
                text += `<li class="slider-menu__pagination-bullet swiper-pagination-bullet active">${
                  menuItem[i - 1]
                }</li>`;
              } else {
                text += `<li class="slider-menu__pagination-bullet swiper-pagination-bullet">${
                  menuItem[i - 1]
                }</li>`;
              }
            }
            text += '</ul>';
            return text;
          },
        },

        navigation: {
          nextEl: this.next,
          prevEl: this.prev,
        },
      });
    } else {
      if (this.slider.is('[data-mobile-destroy]')) {
			  this.breakpointChecker();

        $(window).on('resize', function() {
          that.breakpointChecker();
        });
      } else if (this.slider.is('[data-desktop-destroy]')) {
        this.breakpointCheckerMin();
        $(window).on('resize', function() {
          that.breakpointCheckerMin();
        });
      } else if (this.slider.is('[data-tablet-destroy]')) {
        this.breakpointCheckerTablet();
        $(window).on('resize', function() {
          that.breakpointCheckerTablet();
          
        });

      } 
      else {
        this.enableSwiper();
      }
    }

    if (this.slider.is('[data-slider-counter]')) {
      let counterCurrent = this.slider.parent().find('[data-slider-current]');
      this.currentSlider.on('slideChange', function() {
        that.currentSlider.snapGrid = that.currentSlider.slidesGrid.slice(0);
        counterCurrent.text(that.currentSlider.realIndex + 1);
      });
    }

    if (this.slider.is('[data-slider-in-tab]') && this.slider.closest('[data-tabs-body]').length > 0) {
      let body = this.slider.closest('[data-tabs-body]');
      let link = body.find('[data-tabs-link]');
      link.on('click', function() {
        setTimeout(() => {
          that.currentSlider.update();
          that.currentSlider.snapGrid = that.currentSlider.slidesGrid.slice(0);
        }, 100);
      });
    }

    if (this.slider.is('[data-slider-in-tab]') && this.slider.closest('[data-tabs-body]').length > 0 && this.ww < 1024) {
      let body = this.slider.closest('[data-tabs-body]');
      let tabsInner = body.find('[data-tabs-inner]');

      tabsInner.on('click', function() {
        setTimeout(() => {
          that.currentSlider.update();
          that.currentSlider.snapGrid = that.currentSlider.slidesGrid.slice(0);
        }, 100);
      });
    }
  }

  enableSwiper() {
    const that = this;

    this.currentSlider = new Swiper(this.slider, {
      lazy: false,
      speed: this.speed,
      autoHeight: this.autoHeight,
      loop: this.loop,
      slidesPerView: this.slidesPerView,
      spaceBetween: this.spaceBetween === 0 ? 0 : +this.spaceBetween,
      freeMode: this.freeMode,
      slidesPerColumn: this.slidesPerColumn,
      effect: this.effect,
      autoplay: this.autoplay,
      direction: this.direction,
      simulateTouch: this.touchSwipe,
      mousewheel: {
        enabled: this.mousewheel,
        releaseOnEdges: true,
      },
      slideToClickedSlide: true,
      fadeEffect: {
        crossFade: true,
      },

      pagination: {
        el: this.pagination,
        clickable: true,
        type: this.paginationType,
      },

      navigation: {
        nextEl: this.next,
        prevEl: this.prev,
      },

      on: {
        init() {
          if (that.next.length) {
            let arrowCircle = that.next.find('circle');
            arrowCircle.animate(
              { 'stroke-dashoffset': `0` },
              that.autoplay.delay
            );
            that.next.on('click', function () {
              arrowCircle.css({ strokeDashoffset: 200 });
            });
          }
        },
        slideChange() {
          if (that.next.length) {
            let arrowCircle = that.next.find('circle');
            arrowCircle.stop();
            arrowCircle.css({ strokeDashoffset: 200 });
            arrowCircle.animate(
              { 'stroke-dashoffset': `0` },
              that.autoplay.delay
            );
          }
        },
      },
    });

    this.currentSlider.snapGrid = this.currentSlider.slidesGrid.slice(0);

    setTimeout(() => {
      that.currentSlider.update();
    }, 100);

    if (this.slider.is('[data-slider-vertical-anim]')) {
      this.verticalAnim();
    }

    $(window).on('resize', function() {
      if (that.currentSlider !== undefined && !that.currentSlider.destroyed) {
        that.ww = $(window).width();
        setTimeout(() => {
          that.currentSlider.update();
        }, 100);
      }
    });
  }

  verticalAnim() {
    const that = this;

    if (this.currentSlider !== undefined) {
      $('html').css({'--verticalpercent': '18%'});

      let currentSlide = that.currentSlider.realIndex;

      if (that.slider.closest('[data-slider-achiev-parent]').length > 0) {
        let currentSlide = that.currentSlider.realIndex;

        if (that.slider.closest('[data-slider-achiev-parent]').length > 0) {
          let amountContainer = that.slider.closest('[data-slider-achiev-parent]').find('[data-slider-achiev-amount]');
          let amount = currentSlide + 1;

          amountContainer.text('0' + amount);
          amountContainer.attr('data-slider-achiev-amount', '0' + amount);

          let mainContainer = $(amountContainer[0]);
          let baseHeight = $(amountContainer[1]).height();
          let part = (baseHeight - 10) / that.currentSlider.slides.length;

          mainContainer.css({'height': `${baseHeight - (part * amount)}px`})
        }
      }

      this.currentSlider.on('slideChange', function() {
        let currentSlide = that.currentSlider.realIndex;
        if (!that.currentSlider.destroyed) {
          if (that.slider.closest('[data-slider-achiev-parent]').length > 0) {
            let amountContainer = that.slider.closest('[data-slider-achiev-parent]').find('[data-slider-achiev-amount]');
            let amount = currentSlide + 1;

            amountContainer.text('0' + amount);
            amountContainer.attr('data-slider-achiev-amount', '0' + amount);

            let mainContainer = $(amountContainer[0]);
            let baseHeight = $(amountContainer[1]).height();
            let part = (baseHeight - 10) / that.currentSlider.slides.length;

            mainContainer.css({'height': `${baseHeight - (part * amount)}px`})
          }
        }
      });

      $(window).on('resize', function() {
        if (!that.currentSlider.destroyed) {
          let currentSlide = that.currentSlider.realIndex;
          if (that.slider.closest('[data-slider-achiev-parent]').length > 0) {
            let amountContainer = that.slider.closest('[data-slider-achiev-parent]').find('[data-slider-achiev-amount]');
            let amount = currentSlide + 1;

            amountContainer.text('0' + amount);
            amountContainer.attr('data-slider-achiev-amount', '0' + amount);

            let mainContainer = $(amountContainer[0]);
            let baseHeight = $(amountContainer[1]).height();
            let part = (baseHeight - 10) / that.currentSlider.slides.length;

            mainContainer.css({'height': `${baseHeight - (part * amount)}px`})
          }
        }
      });
    }
  }

  breakpointChecker() {
    this.breakpointMin = $(window).width() >= 1024;
    // if larger viewport and multi-row layout needed
    if (!this.breakpointMin) {

        // clean up old instances and inline styles when available
      if ( this.currentSlider !== undefined ){
        this.currentSlider.destroy(true, true);
      }

      // or/and do nothing
      return;

    // else if a small viewport and single column layout needed
    } else if (this.breakpointMin) {
      // fire small viewport version of swiper
      return this.enableSwiper();
    }

  };

  breakpointCheckerMin() {
    this.breakpointMax = $(window).width() < 1024;
    // if larger viewport and multi-row layout needed
    if (!this.breakpointMax) {
        // clean up old instances and inline styles when available
      if ( this.currentSlider !== undefined ) {
        this.currentSlider.destroy(true, true);
      }

      // or/and do nothing
      return;

    // else if a small viewport and single column layout needed
    } else if (this.breakpointMax) {
      // fire small viewport version of swiper
      return this.enableSwiper();
    }

  };

  breakpointCheckerTablet() {
    this.breakpointMax = $(window).width() > 767 && $(window).width() < 1024;
    // if larger viewport and multi-row layout needed
    if (!this.breakpointMax) {
        // clean up old instances and inline styles when available
      if ( this.currentSlider !== undefined ) {
        this.currentSlider.destroy(true, true);
      }

      // or/and do nothing
      return;

    // else if a small viewport and single column layout needed
    } else if (this.breakpointMax) {
      // fire small viewport version of swiper
      return this.enableSwiper();
    }
  }
}

export { Slider };

$(document).ready(function () {
  let sliders = [];

  if ($('[data-slider]').length > 0) {
    $('[data-slider]').each(function (i) {
      if ($(this).attr('data-slider') != 'loading') {
        sliders[i] = new Slider($(this));
        sliders[i].initialize();
      }
    });
  }
});
