class Banner {

  constructor(elem) {
    this.banner = elem;
	}

  initialize() {

	}
}

export {Banner};

$(window).on('load', () => {
  let banners = [];

  if ($('[data-banner]').length > 0) {
		$('[data-banner]').each(function(i) {
			banners[i] = new Banner($(this));
			banners[i].initialize();
		});
	}
});
