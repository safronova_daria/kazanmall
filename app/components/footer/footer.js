class Footer {
  constructor(elem) {
    this.footer = elem;
  }

  initialize() {
    let footer = this.footer;
    let footerTitle = footer.find('[data-footer-title]');

    footerTitle.on('click', function() {
      $(this).next().toggleClass('is-open');
      $(this).children().last().toggleClass('is-inverted');
    });
  }
}

export { Footer };

$(window).on('load', () => {
  let footerElements = [];
  if ($('[data-footer]').length > 0) {
    $('[data-footer]').each(function (i) {
      footerElements[i] = new Footer($(this));
      footerElements[i].initialize();
    });
  }
});