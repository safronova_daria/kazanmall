import { map } from "jquery";

class Map {

  constructor(elem) {
    this.map = elem;

    this.geoObjectsInit = [];
    this.clusterer;
    this.maps;
    this.isCity = '';
    this.isSearch = '';
    this.isSelect = '';
    this.template = '';
    this.templateСourier = '';
    this.templateOptions = '';
    this.courier = window.courier;
    this.courierCity = [];
    this.res = [];
    this.path = '/';
    this.form = '';
	}

	initialize() {
    let mapZoom = this.map.attr('zoom') || 18;
		let shops = window.shops;
    let storageCoords = window.storageCoords;
    const that = this;

    $(document).ready(() => {
      ymaps.ready(() => {
        this.map.each((index, el) => {
          let load = $(el).data('map');
          let status = false;

          if (load == '') status = true;

          this.loadMap($(el), status, 'init', shops, mapZoom);
        });
      });
    });

    if (process.env.NODE_ENV != 'development'){
      this.path = '../build/'
    }

    if (this.map.closest('[data-map-tabs]').length > 0) {
      let link = this.map.closest('[data-map-tabs]').find('[data-map-tab]')
      let onFootButton = null;
      let carButton = null;

      link.on('click', function() {
        if ($(this).attr('data-map-tab') === 'default') {
          if (!!onFootButton && !!carButton) {
            that.maps.controls.remove(onFootButton);
            that.maps.controls.remove(carButton);
            onFootButton = null;
            carButton = null;
          }
          that.maps.geoObjects.removeAll();
          let centerLat = shops[0].coords.split(',')[0].trim();
          let centerLon = shops[0].coords.split(',')[1].trim();
          that.maps.setCenter([centerLat, centerLon], 17, {
            duration: 1000,
            timingFunction: 'ease-in'
          });
          that.addPlacemark([window.shops[0]]);
        } else if ($(this).attr('data-map-tab') === 'branch') {
          if (!!onFootButton && !!carButton) {
            that.maps.controls.remove(onFootButton);
            that.maps.controls.remove(carButton);
            onFootButton = null;
            carButton = null;
          }
          that.maps.geoObjects.removeAll();
          that.maps.setCenter([54.685996, 75.373970], 4, {
            duration: 1000,
            timingFunction: 'ease-in'
          });
          that.addPlacemark(shops);

        } else if ($(this).attr('data-map-tab') === 'storage') {
          that.maps.geoObjects.removeAll();
          let centerLat = storageCoords[0].coords.split(',')[0].trim();
          let centerLon = storageCoords[0].coords.split(',')[1].trim();
          that.maps.setCenter([centerLat, centerLon], 16, {
            duration: 1000,
            timingFunction: 'ease-in'
          });

          that.addPlacemark(storageCoords);

          if (!carButton && !onFootButton) {
            carButton = new ymaps.control.Button({
              data: {
                content: 'На машине',
              },
              options: {
                layout: ymaps.templateLayoutFactory.createClass(
                  "<div class='map__button {% if state.selected %}map__button--active{% endif %}' title='{{ data.title }}'>" +
                  "{{ data.content }}" +
                  "</div>"
                  ),
                  maxWidth: 150
              }
            });

            onFootButton = new ymaps.control.Button({
              data: {
                content: 'Пешком',
              },
              options: {
                layout: ymaps.templateLayoutFactory.createClass(
                  "<div class='map__button {% if state.selected %}map__button--active{% endif %}' title='{{ data.title }}'>" +
                  "{{ data.content }}" +
                  "</div>"
                  ),
                  maxWidth: 150
              }
            });


            let carRouteIndex = null;
            let onFootRouteIndex = null;

            carButton.events.add('select', function() {
              let carRoute = new ymaps.multiRouter.MultiRoute({
                referencePoints: [ [55.716724, 37.708127],
                [centerLat, centerLon]
                ],
                params: {
                  routingMode: "auto"
                }
              },{
                boundsAutoApply: true,
              })

              that.maps.geoObjects.add(carRoute);
              if (onFootButton.isSelected() && !!onFootRouteIndex) {
                onFootButton.deselect();
              }
              carRouteIndex = that.maps.geoObjects.indexOf(carRoute);
            })

            carButton.events.add('deselect', function() {
              that.maps.geoObjects.remove(that.maps.geoObjects.get(carRouteIndex));
            })

            onFootButton.events.add('select', function() {
              let footRoute = new ymaps.multiRouter.MultiRoute({
                referencePoints: [ [55.708575, 37.728871],
                  [centerLat, centerLon]
                ],
                params: {
                  routingMode: "pedestrian"
                }
              },{
                boundsAutoApply: true,
              })

              that.maps.geoObjects.add(footRoute);
              if (carButton.isSelected() && !!carRouteIndex) {
                carButton.deselect();
              }
              onFootRouteIndex = that.maps.geoObjects.indexOf(footRoute);
            })

            onFootButton.events.add('deselect', function() {
              that.maps.geoObjects.remove(that.maps.geoObjects.get(onFootRouteIndex));
            });

            that.maps.controls.add(carButton, { float: 'none', position: {right: '5px', top: '48px'} });
            that.maps.controls.add(onFootButton, { float: 'none', position: {right: '5px', top: '5px'} });
          }
        }
      });
    }

    if (this.map.closest('[data-map-form]').length > 0) {
      this.form = this.map.closest('[data-map-form]');
      let input = this.form.find('[data-map-input]');

      let cityArray = this.form.find('[data-city]');
      if (cityArray.length > 0) {
        this.form.on('input submit', function(e) {
          e.preventDefault();
          cityArray.addClass('d-none');
          for (let i = 0; i < cityArray.length; i ++) {
            if ($(cityArray[i]).text().toLowerCase().includes(input.val().toLowerCase())) {
              $(cityArray[i]).removeClass('d-none');
            }
          }
        });
      }
    }
	}

  loadMap(el, status, func, shops, mapZoom) {
    if (status) {
      if (func == 'init') {
        if (el.children().length < 1) {
          this.init(el.attr('id'), shops, mapZoom);
        }
      }
    }
  }

  init (id, shops, mapZoom) {
    let centerLat = 0;
    let centerLon = 0;
    const that = this;

    for (let i = 0; i < shops.length; i++) {
      centerLat += parseFloat(shops[i].coords.split(',')[0]);
      centerLon += parseFloat(shops[i].coords.split(',')[1]);
    }

    this.maps = new ymaps.Map(id, {
      center: [
        parseFloat(centerLat / shops.length),
        parseFloat(centerLon / shops.length)
      ],
      zoom: mapZoom,
      controls: ['zoomControl']
      }, {
      searchControlProvider: 'yandex#search',
      yandexMapDisablePoiInteractivity: true,
    });

    if (!this.map.is('[data-zoom]')) {
      this.maps.behaviors.disable('scrollZoom');
      let zoomContainer = that.map.parent().find('[data-map-zoom]');

      let zoomDisabled = true;

      this.maps.events.add('contextmenu', function() {
        if (zoomDisabled) {
          that.maps.behaviors.enable('scrollZoom');
          zoomContainer.removeClass('active');
          zoomDisabled = false;
        }
      });

      let zoomTimeOut = null;

      this.maps.events.add('wheel', function(e) {
        if (zoomDisabled) {
          if (!!zoomTimeOut) clearTimeout(zoomTimeOut);
          zoomContainer.addClass('active');
          zoomTimeOut = setTimeout(function() {
            zoomContainer.removeClass('active');
          }, 1000);
        }
      });
    } 

    // Кастомный кластер
    let clusterIcons = [{
      href: '',
      size: [48, 48],
      offset: [-24, -24]
    }];

    this.clusterer = new ymaps.Clusterer({
      clusterIcons: clusterIcons,
      openBalloonOnClick: false,
    });


    this.addPlacemark(shops);

    if (this.map.closest('[data-map-tabs]').length > 0) {
      this.maps.geoObjects.removeAll();
      let centerLat = shops[0].coords.split(',')[0].trim();
      let centerLon = shops[0].coords.split(',')[1].trim();
      this.addPlacemark([window.shops[0]]);
      this.maps.setCenter([centerLat, centerLon], 17);
    }

    if (this.map.is('[data-map-default]')) {
      that.maps.geoObjects.removeAll();
      let centerLat = shops[0].coords.split(',')[0].trim();
      let centerLon = shops[0].coords.split(',')[1].trim();
      that.maps.setCenter([centerLat, centerLon], 17, {
        duration: 1000,
        timingFunction: 'ease-in'
      });
      that.addPlacemark([window.shops[0]]);
    }

    if (this.map.is('[data-map-storage]')) {
      let onFootButton = null;
      let carButton = null;

      that.maps.geoObjects.removeAll();
      let centerLat = storageCoords[0].coords.split(',')[0].trim();
      let centerLon = storageCoords[0].coords.split(',')[1].trim();
      that.maps.setCenter([centerLat, centerLon], 16, {
        duration: 1000,
        timingFunction: 'ease-in'
      });

      that.addPlacemark(storageCoords);

      carButton = new ymaps.control.Button({
        data: {
          content: 'На машине',
        },
        options: {
          layout: ymaps.templateLayoutFactory.createClass(
            "<div class='map__button {% if state.selected %}map__button--active{% endif %}' title='{{ data.title }}'>" +
            "{{ data.content }}" +
            "</div>"
            ),
            maxWidth: 150
        }
      });

      onFootButton = new ymaps.control.Button({
        data: {
          content: 'Пешком',
        },
        options: {
          layout: ymaps.templateLayoutFactory.createClass(
            "<div class='map__button {% if state.selected %}map__button--active{% endif %}' title='{{ data.title }}'>" +
            "{{ data.content }}" +
            "</div>"
            ),
            maxWidth: 150
        }
      });


      let carRouteIndex = null;
      let onFootRouteIndex = null;

      carButton.events.add('select', function() {
        let carRoute = new ymaps.multiRouter.MultiRoute({
          referencePoints: [ [55.716724, 37.708127],
          [centerLat, centerLon]
          ],
          params: {
            routingMode: "auto"
          }
        },{
          boundsAutoApply: true,
        })

        that.maps.geoObjects.add(carRoute);
        if (onFootButton.isSelected() && !!onFootRouteIndex) {
          onFootButton.deselect();
        }
        carRouteIndex = that.maps.geoObjects.indexOf(carRoute);
      })

      carButton.events.add('deselect', function() {
        that.maps.geoObjects.remove(that.maps.geoObjects.get(carRouteIndex));
      })

      onFootButton.events.add('select', function() {
        let footRoute = new ymaps.multiRouter.MultiRoute({
          referencePoints: [ [55.708575, 37.728871],
            [centerLat, centerLon]
          ],
          params: {
            routingMode: "pedestrian"
          }
        },{
          boundsAutoApply: true,
        })

        that.maps.geoObjects.add(footRoute);
        if (carButton.isSelected() && !!carRouteIndex) {
          carButton.deselect();
        }
        onFootRouteIndex = that.maps.geoObjects.indexOf(footRoute);
      })

      onFootButton.events.add('deselect', function() {
        that.maps.geoObjects.remove(that.maps.geoObjects.get(onFootRouteIndex));
      });

      that.maps.controls.add(carButton, { float: 'none', position: {right: '5px', top: '48px'} });
      that.maps.controls.add(onFootButton, { float: 'none', position: {right: '5px', top: '5px'} });
    }

    if (this.map.is('[data-map-branch]')) {
      that.maps.geoObjects.removeAll();
      that.maps.setCenter([54.685996, 75.373970], 2, {
        duration: 1000,
        timingFunction: 'ease-in'
      });
      that.addPlacemark(shops);
    }
  }

  addPlacemark(shops) {
    const that = this;
    let BalloonContentLayoutClass = [];
    let pathAssent = '';

    this.geoObjectsInit = [];

    let zoom = that.maps.getZoom();
    let iconImageHref = zoom > 10 ? `${that.path}assets/images/required/map-marker.svg` : `${that.path}assets/images/required/dot.svg`
    let iconImageSize = zoom > 10 ? [126, 126] : [16 ,16]
    let iconImageOffset = zoom > 10 ? [-63, -63] : [-8 ,-8]

    let getPointOptions = () => {
      // mark
      return {
        iconLayout: 'default#image',
        iconImageHref: iconImageHref,
        iconImageSize: iconImageSize,
        iconImageOffset: iconImageOffset,
        hideIconOnBalloonOpen: false,
        balloonPanelMaxMapArea: 0,
        balloonMaxWidth: 236,
        balloonMinWidth: 236,
        balloonOffset: [-65, -60]
      };
    };

    for (let i = 0; i < shops.length; i++) {
      let pointData = '';
      let pointOptions = getPointOptions();
      let id = shops[i].id;
      let templateBallon = '';

      BalloonContentLayoutClass[i] = ymaps.templateLayoutFactory.createClass(templateBallon);

      pointOptions.balloonContentLayout = BalloonContentLayoutClass[i],
      pointOptions.id = id;

      let mark = new ymaps.Placemark([shops[i].coords.split(',')[0], shops[i].coords.split(',')[1]], pointData, pointOptions );

      this.geoObjectsInit.push(mark);
      // this.clusterer.removeAll();

      // this.clusterer.add(this.geoObjectsInit)
      // this.maps.geoObjects.add(this.clusterer);
      this.maps.geoObjects.add(mark);

      if (process.env.NODE_ENV === 'development') {
        pathAssent = '/'
      } else {
        pathAssent = '/local/frontend/'
      }
    }

  }
}

export {Map};

$(window).on('load', () => {
  let map = [];

  if ($('[data-map]').length > 0) {
		$('[data-map]').each(function(i) {
			map[i] = new Map($(this));
			map[i].initialize();
		});
	}
});
