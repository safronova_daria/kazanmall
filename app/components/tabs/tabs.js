class Tabs {
    constructor(elem) {
        this.tabs = elem;
    }

    initialize() {
        const that = this;
        let tabs = this.tabs;
        let container = this.tabs.closest('[data-tabs-container]');

        let tabsLinks = tabs.children();
        let tabsBody = tabs.next();
        let tabsItem = tabsBody.children();

        if (container.length > 0) {
            tabsBody = container.find('[data-tabs-body]');
            tabsItem = tabsBody.children();
        }

        tabsLinks.on('click', function(e) {
            let el = $(this);
            let index = el.index();

            tabsLinks.removeClass('active');
            el.addClass('active');

            tabsItem.removeClass('active');
            tabsItem.eq(index).addClass('active');

            if (tabs.parent().is('[data-tabs-body-anim]')) {
                let bodyItem = tabsBody.find('[data-tabs-item]');
                let bodyInner = $(bodyItem[index]).find('[data-tabs-inner]');

                if ($(this).is('[data-no-map]')) {
                    $(this)
                        .closest('[data-map-tabs]')
                        .find('[data-map]')
                        .addClass('d-none');
                } else {
                    $(this)
                        .closest('[data-map-tabs]')
                        .find('[data-map]')
                        .removeClass('d-none');
                }
            }

            e.preventDefault();
        });

        if (container.find('[data-tabs-link]').length > 0) {
            let link = tabsBody.find('[data-tabs-link]');

            let mobileTabs = tabsBody.find('[data-tabs-inner]');
            let mobileTabsContent = tabsBody.find('[data-tabs-content]');

            tabsItem.on('click', function() {
                $(this).toggleClass('mobileshow');
                $(this).find('[data-tabs-inner-container]').toggleClass('hide');
            });

            mobileTabsContent.on('click', function(e) {
                e.stopPropagation();
            });

            mobileTabs.on('click', function(e) {
                e.stopPropagation();
                $(this).toggleClass('mobileshow');
                $(this).find('[data-tabs-content]').toggleClass('active');
            });

            link.on('click', function(e) {
                e.preventDefault();
                $(this)
                    .closest('[data-tabs-item]')
                    .find('[data-tabs-link]')
                    .removeClass('active');
                $(this).addClass('active');

                let bodyInner = $(this)
                    .closest('[data-tabs-item]')
                    .find('[data-tabs-inner]');

                let index = $(this).attr('data-tabs-link');

                bodyInner.removeClass('show');
                setTimeout(() => {
                    bodyInner.removeClass('active');
                }, 100);
                $(bodyInner[+index]).addClass('show');
                setTimeout(() => {
                    $(bodyInner[+index]).addClass('active');
                }, 100);
            });

            if (tabsItem.find('[data-search-city-input]').length > 0) {
                let input = tabsItem.find('[data-search-city-input]');
                let form = input.closest('form');
                let currentItem = input.closest('[data-tabs-body]');

                let cityArray = currentItem.find('[data-city]');

                form.on('input submit', function(e) {
                    e.preventDefault();
                    cityArray.addClass('d-none');
                    for (let i = 0; i < cityArray.length; i++) {
                        if (
                            $(cityArray[i])
                            .text()
                            .toLowerCase()
                            .includes(input.val().toLowerCase())
                        ) {
                            $(cityArray[i]).removeClass('d-none');
                        }
                    }
                });
            }

            if (tabsItem.find('[data-search-news-input]').length > 0) {
                let input = tabsItem.find('[data-search-news-input]');
                let form = input.closest('form');
                let currentItem = input.closest('[data-tabs-body]');

                let newsArray = currentItem.find('[data-news]');
                let newsContainerArray = currentItem.find(
                    '.find-page__found-item-block'
                );
                let notFoundLabel = currentItem.find('[data-not-found]')
                let findButton = form.find('.input-news__button');

                findButton.on('click', function() {
                    newsContainerArray.addClass('d-none');
                    for (let i = 0; i < newsArray.length; i++) {
                        if (
                            $(newsArray[i])
                            .text()
                            .toLowerCase()
                            .includes(input.val().toLowerCase())
                        ) {
                            $(newsContainerArray[i]).unhighlight();
                            $(newsContainerArray[i]).highlight(input.val());
                            $(newsContainerArray[i]).removeClass('d-none');
                        }
                    }

                    if (newsContainerArray.is(':visible')) {
                        notFoundLabel.addClass('is-hide');
                    } else {
                        notFoundLabel.removeClass('is-hide');
                    }
                });

                form.on('submit', function(e) {
                    e.preventDefault();
                    findButton.trigger('click');
                });
            }
        }
    }
}

export { Tabs };

$(function() {
    window.Tabs = Tabs;
});

$(window).on('load', () => {
    let tabs = [];

    if ($('[data-tabs-head]').length > 0) {
        $('[data-tabs-head]').each(function(i) {
            tabs[i] = new Tabs($(this));
            tabs[i].initialize();
        });
    }
});