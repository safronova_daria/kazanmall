import Swiper from 'swiper';

class NestedSlider {
    constructor(elem, index = 0) {
        this.slider = elem;
        this.index = index;
    }

    initialize() {
        var el = this.slider;
        let slider = undefined;

        slider = new Swiper(el, {
            spaceBetween: 200,
            speed: 1500,
            allowTouchMove: false,
            effect: 'fade',
            fadeEffect: {
                crossFade: true,
            },
        });
    }
}

class NestedHeaderSlider {
    constructor(elem, index = 0) {
        this.slider = elem;
        this.index = index;
    }

    initialize() {
        var el = this.slider;
        let breakpoints = {
            1024: {
                spaceBetween: -50,
            },
            1440: {
                spaceBetween: 155,
            },
            1700: {
                spaceBetween: 265,
            },
            1920: {
                spaceBetween: 0,
            },
        }

        if ($(this).attr('data-slider-nested-header') === 'single') {
            breakpoints = {}
        }

        let slider = new Swiper(el, {
            loop: true,
            speed: 1500,
            slidesPerView: 'auto',
            spaceBetween: 0,
            slideToClickedSlide: true,
            allowTouchMove: false,
            breakpoints: breakpoints,
        });

        if (this.slider.closest('[data-slider-container]').find('[data-slider-outer]').length > 0) {
            slider.snapGrid = slider.slidesGrid.slice(0);

            this.outerSlider = this.slider.closest('[data-slider-container]').find('[data-slider-outer]');
            let sliderOuterItems = this.outerSlider.find('[data-slider-outer-item]');

            slider.on('slideChange', function() {
                sliderOuterItems.removeClass('show active');
                // setTimeout(() => {
                //     $(sliderOuterItems[slider.realIndex - 1]).removeClass('active');
                // }, 300);

                $(sliderOuterItems[slider.realIndex]).addClass('active');
                setTimeout(() => {
                    $(sliderOuterItems[slider.realIndex]).addClass('show');
                }, 200);
            });
        }
    }

}

export { NestedSlider };
export { NestedHeaderSlider };

$(document).ready(function() {
    let slidersInner = [];
    let slidersOuter = [];

    let currentInnerSlide = null;
    let currentOuterSlide = null;

    if ($('[data-slider-nested]').length > 0) {
        $('[data-slider-nested]').each(function(i) {
            if ($(this).attr('data-slider-nested') != 'loading') {
                slidersInner[i] = new NestedSlider($(this));
                slidersInner[i].initialize();
            }
        });
    }

    if ($('[data-slider-nested-header]').length > 0) {
        $('[data-slider-nested-header]').each(function(i) {
            if ($(this).attr('data-slider-nested-header') !== 'loading') {
                slidersOuter[i] = new NestedHeaderSlider($(this));
                slidersOuter[i].initialize();
            }

            if ($(this).attr('data-slider-nested-header') !== 'single') {
                currentInnerSlide = slidersInner[i].slider[0].swiper;
                currentOuterSlide = slidersOuter[i].slider[0].swiper;

                currentOuterSlide.on('slideNextTransitionStart', function() {
                    currentOuterSlide.realIndex === 1 ?
                        currentInnerSlide.slideNext() :
                        currentInnerSlide.slidePrev();
                });
            }
        });
    }
});