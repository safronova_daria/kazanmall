class SliderSplit {
  constructor(elem) {
    this.slider = elem;
    this.links = elem.find('[slider-split-link]');
    this.body = elem.find('[slider-split-body]');
  }

  initialize() {
    const that = this;

    this.links.on('click', function() {
      let currentIndex = $(this).attr('slider-split-link');

      that.links.removeClass('active');
      that.body.removeClass('active show');

      $(this).addClass('active');
      $(that.body[currentIndex]).addClass('active');
      setTimeout(() => {
        $(that.body[currentIndex]).addClass('show');
      }, 0);
    });
  }
}

export {SliderSplit}

$(document).ready(function () {
  let sliders = [];

  if ($('[slider-split]').length > 0) {
    $('[slider-split]').each(function (i) {
      if ($(this).attr('slider-split') != 'loading') {
        sliders[i] = new SliderSplit($(this));
        sliders[i].initialize();
      }
    });
  }
});