class Pagination {
  constructor(elem, index = 0) {
    this.pagination = elem;
    this.index = index;
  }

  initialize() {
    let paginationEl = this.pagination;
    let activeElement = null;
    let paginationItem = paginationEl.find('.pagination__item');

    let startButton = paginationEl.find('.pagination__btn-start');
    let prevButton = paginationEl.find('.pagination__btn-prev');
    let endButton = paginationEl.find('.pagination__btn-end');
    let nextButton = paginationEl.find('.pagination__btn-next');
    let prevArrow = paginationEl.find('.pagination__prev');
    let nextArrow = paginationEl.find('.pagination__next');

    var windowWidth = $(window).width();

    paginationItem.each(function (index) {
      $(this).on('click', function (e) {
        e.preventDefault();
        activeElement = index;
        changePage(activeElement);
      });
    });

    function changePage(page) {
      paginationItem.addClass('is-hide').removeClass('is-active');

      $(paginationItem[page]).addClass('is-active').removeClass('is-hide');

      showNeighboringElement(page); //2 соседних элемента по обе стороны от активного

      //Первый и последний элемент с точками
      paginationItem
        .first()
        .removeClass('is-hide')
        .next()
        .removeClass('is-hide');
      paginationItem
        .last()
        .removeClass('is-hide')
        .prev()
        .removeClass('is-hide');

      if (page < 4) {
        paginationItem.first().next().addClass('is-hide');
        paginationItem.slice(0, 5).removeClass('is-hide');
      }

      if (page > paginationItem.length - 5) {
        paginationItem.last().prev().addClass('is-hide');
        paginationItem.slice(-5).removeClass('is-hide');
      }

      if (page === paginationItem.length - 1)
        endButton.parent().addClass('is-hide');
      else endButton.parent().removeClass('is-hide');

      if (page === 0) startButton.parent().addClass('is-hide');
      else startButton.parent().removeClass('is-hide');
    }

    startButton.on('click', function () {
      activeElement = 0;
      startButton.parent().addClass('is-hide');
      endButton.parent().removeClass('is-hide');
      changePage(activeElement);
    });

    prevButton.add(prevArrow).on('click', function (e) {
      e.preventDefault();
      activeElement <= 0 ? (activeElement = 0) : (activeElement -= 1);
      changePage(activeElement);
    });

    nextButton.add(nextArrow).on('click', function (e) {
      e.preventDefault();
      activeElement >= paginationItem.length - 1
        ? (activeElement = paginationItem.length - 1)
        : (activeElement += 1);
      changePage(activeElement);
    });

    endButton.on('click', function () {
      activeElement = paginationItem.length - 1;
      startButton.parent().removeClass('is-hide');
      endButton.parent().addClass('is-hide');
      changePage(activeElement);
    });

    function showNeighboringElement(index) {
      if (windowWidth >= 768) {
        $(paginationItem[index + 1]).removeClass('is-hide');
        $(paginationItem[index + 2]).removeClass('is-hide');
        $(paginationItem[index - 1]).removeClass('is-hide');
        $(paginationItem[index - 2]).removeClass('is-hide');
      }
      else {
        $(paginationItem[index + 1]).removeClass('is-hide');
        $(paginationItem[index - 1]).removeClass('is-hide');
      }
    }
  }
}

export { Pagination };
let pagintaionElement = [];
$(window).on('load', () => {
  $('.pagination').each(function (i) {
    pagintaionElement[i] = new Pagination($(this));
    pagintaionElement[i].initialize();
  });
});
