import 'magnific-popup';
import {Slider} from '../slider/slider'
// import {Zoom} from '../zoom/zoom'
// import {Input} from "../forms/input/input";

class Popup {

  constructor(elem) {
    this.popup = elem;
    this.close = $('[data-popup-close]');
    this.closeBtn = $('[data-popup-btn]');
    this.init = false;
    this.sliders = [];
	}

  initialize() {
    let popup = this.popup;
    const that = this;

    popup.magnificPopup({
			type:'inline',
      fixedBgPos: true,
			midClick: true,
      closeMarkup: '<button title="%title%" type="button" class="mfp-close popup__close close"></button>',
      removalDelay: 200, //delay removal by X to allow out-animation
      callbacks: {
        beforeOpen: function() {
          if (this.st.el.attr('href') == '#popup-review') {
            if ($(window).width() > 767) {
              this.st.mainClass = 'mfp-zoom-in mfp-modal';
            } else {
              this.st.mainClass = 'mfp-zoom-in';
            }
          } else if (this.st.el.find('[data-popup-video]')) {
            if ($(window).width() > 767) {
              this.st.mainClass = 'mfp-zoom-in mfp-modal';
              this.bgOverlay.addClass('mfp-video');
            } else {
              this.st.mainClass = 'mfp-zoom-in';
            }
          } else if (this.st.el.is('[data-popup-gallery]')) {
            this.st.mainClass = 'mfp-modal';
          } else {
            this.st.mainClass = 'mfp-zoom-in';
          }
          $('html').attr('style', 'overflow: hidden;')
        },
				open: function() {
          let id = this.st.el.attr('href');
          let el = this.st.el;
          let index = 0;

          if (el.parent().hasClass('swiper-slide')) {
            index = el.parent().attr('data-swiper-slide-index');
          } else {
            if (el.attr('data-popup') != '') {
              index = parseInt(el.attr('data-popup'));
            } else {
              index = el.index();
            }
          }

          if ($(id).find('[data-find-city]').length > 0) {
            let inputs = [];

            $(id).find('[data-find-city]').each(function(i) {
              inputs[i] = new Input($(this));
              inputs[i].initializeCity();
            });
          }

          if ($(id).find('[data-slider="loading"]').length > 0) {
            let sliders = [];

            $(id).find('[data-slider="loading"]').each(function(i) {
              sliders[i] = new Slider($(this), index);
              sliders[i].initialize();
            });
          }

          if ($(id).find('[data-popup-gallery]').length > 0) {
            $(id).find('[data-popup-gallery]').each(function(i) {
              $('body').addClass('overflow');
              $('html').addClass('overflow');
              that.sliders[i] = new Slider($(this), index);
              that.sliders[i].initialize();
            });
          }

          if ($(id).find('[data-slider-popup]').length > 0) {
            let slidersPopup = [];

            $(id).find('[data-slider-popup]').each(function(i) {
              slidersPopup[i] = new Slider($(this), index);
              slidersPopup[i].slidersPopup();
            });

            if ($(id).find('[data-zoom]').length > 0) {
              let zoom = [];

              $(id).find('[data-zoom]').each(function(i) {
                zoom[i] = new Zoom($(this));
                zoom[i].initialize();
              });
            }
          }
				},
        close: function() {
          let id = this.st.el.attr('href');
          let el = this.st.el;
          let index = 0;
          $('html').removeAttr('style')
          if ($(id).find('[data-popup-gallery]').length > 0) {
            $(id).find('[data-popup-gallery]').each(function(i) {
              that.sliders[i].destroy();
              $('body').removeClass('overflow');
              $('html').removeClass('overflow');
            });
          }
        }
      }
		});

    (this.close, this.closeBtn).on('click', function(e) {
      $.magnificPopup.close();

      e.preventDefault();
    });
	}

  initializeRightPopup() {
    let popup = this.popup;

    popup.magnificPopup({
			type:'inline',
      fixedBgPos: true,
			midClick: true,
      closeMarkup: '<button title="%title%" type="button" class="mfp-close popup__close close"></button>',
      removalDelay: 500, //delay removal by X to allow out-animation
      callbacks: {
        beforeOpen: function() {
					this.st.mainClass = 'mfp-right-left';
          $('html').attr('style', 'overflow: hidden;')
        },
				open: function() {
          let id = this.st.el.attr('href');
          let isRegistration = this.st.el.hasClass("registration");
          let index = 0;

          if ($(id).find('[data-slider]').length > 0) {
            let sliders = [];

            $(id).find('[data-slider]').each(function(i) {
              sliders[i] = new Slider($(this), index);
              if (!$(id).find('[data-slider]').hasClass('swiper-container-initialized')) {
                sliders[i].initialize();
              }
            });
          }

          if ($(id).find('[data-slider-popup]').length > 0) {
            let slidersPopup = [];

            $(id).find('[data-slider-popup]').each(function(i) {
              slidersPopup[i] = new Slider($(this), index);
              slidersPopup[i].slidersPopup();
            });

            if ($(id).find('[data-zoom]').length > 0) {
              let zoom = [];

              $(id).find('[data-zoom]').each(function(i) {
                zoom[i] = new Zoom($(this));
                zoom[i].initialize();
              });
            }
          }
          if (id == "#popup-auth"){
            if (isRegistration){
              $(id).find("[data-tabs-head] a").last().trigger("click");
            }else{
              $(id).find("[data-tabs-head] a").first().trigger("click");
            }
          }
				},
        close: function() {
          $('html').removeAttr('style')
        }
      }
		});

    this.close.on('click', function(e) {
      $.magnificPopup.close();

      e.preventDefault();
    });
	}

  initializePopupImg() {
    let popup = this.popup;

    popup.magnificPopup({
			type:'image',
			image: {
				markup: '<div class="mfp-figure popup--figure mfp-with-anim">'+
						'<button title="%title%" type="button" class="mfp-close popup__close"></button>'+
						'<div class="mfp-img"></div>'+
						'<div class="mfp-bottom-bar">'+
						'<div class="mfp-title"></div>'+
						'<div class="mfp-counter"></div>'+
						'</div>'+
						'</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button

				cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor.

				titleSrc: 'title', // Attribute of the target element that contains caption for the slide.
				// Or the function that should return the title. For example:
				// titleSrc: function(item) {
				//   return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
				// }

				verticalFit: true, // Fits image in area vertically

				tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
			},
			callbacks: {
        beforeOpen: function() {
            this.st.mainClass = 'mfp-zoom-in';
            $('html').attr('style', 'overflow: hidden;')
        },
        close: function() {
          $('html').removeAttr('style')
        }
      }
		});
  }
}

export {Popup};

$(function() {
  window.Popup = Popup;
});

$(window).on('load', () => {
  let popups = [];
  let popupsImg = [];

  if ($('[data-popup]').length > 0) {
		$('[data-popup]').each(function(i) {
			popups[i] = new Popup($(this));
			popups[i].initialize();
		});
	}

  if ($('[data-popup-right]').length > 0) {
		$('[data-popup-right]').each(function(i) {
			popups[i] = new Popup($(this));
			popups[i].initializeRightPopup();
		});
	}

  if ($('[data-popup-img]').length > 0) {
		$('[data-popup-img]').each(function(i) {
			popupsImg[i] = new Popup($(this));
			popupsImg[i].initializePopupImg();
		});
	}
});
