class CityList {
    constructor(elem) {
        this.cityList = elem;
    }

    initialize() {
        let cityList = this.cityList;
        let cities = cityList.find('.city-list__item');
        let citiesImgList = cityList.find('[data-cityImg]');
        let findButton = cityList.find('.input-contacts__button');

        cities.each(function(index) {
            $(this).on('mouseover click', function() {
                citiesImgList.removeClass('visibleImg');
                $(citiesImgList[index]).addClass('visibleImg');
                cities.removeClass('city-list__item--active');
                $(this).addClass('city-list__item--active');
            });
        });

        if (cityList.find('[data-search-city-input]').length > 0) {
            let input = cityList.find('[data-search-city-input]');
            let form = input.closest('form');
            let cityNameArray = cityList.find('.city-list__city-name');

            findButton.on('click', function() {
                cities.addClass('d-none');

                for (let i = 0; i < cityNameArray.length; i++) {
                    if (
                        $(cityNameArray[i])
                        .text()
                        .toLowerCase()
                        .includes(input.val().toLowerCase())
                    ) {
                        $(cities[i]).removeClass('d-none');
                    }
                }

                for (let i = 0; i < cityNameArray.length; i++) {
                    if ($(cities[i]).is(':visible')) {
                        citiesImgList.removeClass('visibleImg');
                        cities.removeClass('city-list__item--active');
                        break;
                    }
                }

                if (cities.is(':visible')) {
                    cityList.find('.city-list__not-found').addClass('is-hide');
                } else {
                    cityList.find('.city-list__not-found').removeClass('is-hide');
                    citiesImgList.removeClass('visibleImg');
                }
            });

            form.on('input submit', function(e) {
                e.preventDefault();
                findButton.trigger('click');
            });
        }
    }
}

export { CityList };

$(window).on('load', () => {
    let cityLists = [];

    if ($('[data-city-list]').length > 0) {
        $('[data-city-list]').each(function(i) {
            cityLists[i] = new CityList($(this));
            cityLists[i].initialize();
        });
    }
});
