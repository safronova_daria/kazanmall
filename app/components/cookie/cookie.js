$(document).ready(function () {
  if (sessionStorage.getItem('cookie') !== 'true') {
    setTimeout(() => {
      $('[data-cookie]').removeClass('is-hide');
    }, 1000);
    sessionStorage.setItem('cookie', 'true');
  }

  $('[data-cookie-btn]').on('click', function () {
    $(this).closest('[data-cookie]').addClass('is-hide');
  });
});
