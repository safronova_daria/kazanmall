class SliderLine {
  constructor(elem, index = 0) {
    this.slider = elem;
    this.container = this.slider.parent();
    this.items = this.slider.find('[data-slider-line-item]');
    this.topLine = this.container.find('[data-slider-line-top]');
    this.bottomLine = this.container.find('[data-slider-line-bottom]');
    this.outerContainer = this.slider.closest('[data-slider-line-container]');
    this.outerBlocks = this.outerContainer.find('[data-slider-line-block]');
  }

  initialize() {
    const that = this;
    this.bottomLine.css({'width': $(this.items[0]).width()});

    this.items.on('click', function(e) {
      let elem = $(this);
      that.bottomLine.css({'width': elem.width() + elem.offset().left - $(that.items[0]).offset().left});
      that.items.removeClass('active');
      $(elem).addClass('active');

      let ww = $(window).width();
      let index = $(elem).attr('data-slider-line-item');
      let margin = ($(that.items[1]).offset().left - $(that.container).offset().left) - 
        ($(that.items[0]).offset().left - $(that.container).offset().left + $(that.items).width());

      let sliderWidth = (that.items.width() + margin) * that.items.length;

      if (sliderWidth > ww) {
        let parts = Math.ceil(sliderWidth / ww) + 1;
        let itemPart = Math.ceil(that.items.length / parts);
        let clickedPart = Math.floor((+index) / itemPart);
        let transform = 0;

        if (clickedPart > 0) {
          transform = ($(that.items).width() + margin) * (clickedPart * itemPart) - ($(that.items).width()  / 2);
        }

        that.slider.css({'transform': `translateX(-${transform}px)`});
        that.bottomLine.css({'transform': `translateX(-${transform}px)`});
      }

      that.outerBlocks.removeClass('active show');
      $(that.outerBlocks[index]).addClass('active');
      setTimeout(() => {
        $(that.outerBlocks[index]).addClass('show');
      }, 0);
    });
  }
}

export { SliderLine };

$(document).ready(function () {
  let sliders = [];

  if ($('[data-slider-line]').length > 0) {
    $('[data-slider-line]').each(function (i) {
      if ($(this).attr('data-slider-line') != 'loading') {
        sliders[i] = new SliderLine($(this));
        sliders[i].initialize();
      }
    });
  }
});
