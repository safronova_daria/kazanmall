class Dropdown {
    constructor(elem) {
        this.dropdown = elem;
    }

    initialize() {
        let dropdown = this.dropdown;
        let dropdownItems = dropdown.find('.dropdown__text>a');
        let chosenItem = dropdown.find('.dropdown__chosen-item');
        let dropdownCheckBox = dropdown.find('.dropdown__checkbox');
        let currentLanguage = window.location.href.split('/');
        let newLanguage;
        let isDefaultLanguage = dropdownItems
            .toArray()
            .find((x) => x.text.trim().toLowerCase() == currentLanguage[3]);

        dropdownItems.each(function() {
            $(this).on('click', function(e) {
                e.preventDefault();
                chosenItem[0].firstChild.nodeValue = $(this).text();
                chosenItem.click();
                if (dropdown.is('[data-language]')) {
                    if (
                        isDefaultLanguage &&
                        $(this).text().trim().toLowerCase() !=
                        $(isDefaultLanguage)[0].text.trim().toLowerCase()
                    ) {
                        currentLanguage[3] = $(this).text().trim().toLowerCase();
                        newLanguage = currentLanguage.join('/');
                        window.location.href = newLanguage;
                    }

                    if (!isDefaultLanguage &&
                        $(this).text().trim().toLowerCase() != 'ru'
                    ) {
                        currentLanguage.splice(3, 0, $(this).text().trim().toLowerCase());
                        newLanguage = currentLanguage.join('/');
                        window.location.href = newLanguage;
                    }

                    if (
                        isDefaultLanguage &&
                        $(this).text().trim().toLowerCase() == 'ru'
                    ) {
                        currentLanguage.splice(3, 1);
                        newLanguage = currentLanguage.join('/');
                        window.location.href = newLanguage;
                    }
                }
            });
        });

        if (dropdown.is('[data-language]')) {
            chosenItem[0].firstChild.nodeValue =
                dropdown.attr('data-language').toUpperCase() == 'S1' ?
                'RU' :
                dropdown.attr('data-language').toUpperCase();
        }

        $(document).click(function(e) {
            if (!$(e.target).closest('[data-dropdown]').is('.is-open')) {
                $('[data-dropdown]').removeClass('is-open');
                dropdownCheckBox.prop('checked', false);
            }
        });

        $(dropdown).change(function(e) {
            $(this).toggleClass('is-open');
            dropdownCheckBox.prop('checked', true);
        });
    }
}

export { Dropdown };

$(document).ready(function() {
    let dropdownElement = [];
    if ($('[data-dropdown]').length > 0) {
        $('[data-dropdown]').each(function(i) {
            dropdownElement[i] = new Dropdown($(this));
            dropdownElement[i].initialize();
        });
    }

});