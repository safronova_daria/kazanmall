<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$file = $request->getFile('file');
$filePath = pathinfo($file['name']);
$fileName = $filePath['filename'];
$fileExt = $filePath['extension'];
$newFileName = md5(time().$fileName).'.'.$fileExt;
$allowedFileExtensions = ['jpg', 'png', 'jpeg'];

if (in_array($fileExt, $allowedFileExtensions) && $file['size'] <= 5242880) {
    $uploadFileDir = $_SERVER['DOCUMENT_ROOT'].'local/frontend/build/assets/images/example/upload/';
    $destPath = $uploadFileDir.$newFileName;

    if (!is_dir($uploadFileDir)) {
        mkdir($uploadFileDir);
    }

    if (move_uploaded_file($file['tmp_name'], $destPath)) {
        echo $destPath;
    }
}
