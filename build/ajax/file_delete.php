<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

if ($request->isAjaxRequest()) {
    $file = $request->getPost('filePath');
    $filePath = pathinfo($file);

    if ($filePath['dirname'] == $_SERVER['DOCUMENT_ROOT'].'local/frontend/build/assets/images/example/upload/') {
        unlink($file);

        echo json_encode(['status' => 'ok']);
    }
}
